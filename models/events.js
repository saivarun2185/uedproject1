var mongoose = require('mongoose');

// Movie Schema
var eventSchema = mongoose.Schema({
    eventname: {
        type: String
    },
    starttime: {
        type: String
    },
    endtime: {
        type: String
    },
    eventdate: {
        type: String
    },
    venue: {
        type: String
    },
    eventdesc: {
        type: String
    }
    
});

var eventSchema = module.exports = mongoose.model('event', eventSchema);